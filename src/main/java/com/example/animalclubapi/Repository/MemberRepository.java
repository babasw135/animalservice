package com.example.animalclubapi.Repository;

import com.example.animalclubapi.Entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {

}
