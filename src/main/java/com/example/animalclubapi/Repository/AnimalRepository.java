package com.example.animalclubapi.Repository;


import com.example.animalclubapi.Entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalRepository extends JpaRepository<Animal, Long> {
}
