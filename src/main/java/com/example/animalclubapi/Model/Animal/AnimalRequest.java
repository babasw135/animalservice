package com.example.animalclubapi.Model.Animal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnimalRequest {
    private String animal;
}
