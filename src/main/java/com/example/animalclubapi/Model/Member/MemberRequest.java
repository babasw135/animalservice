package com.example.animalclubapi.Model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRequest {
    private String name;
    private String phoneNumber;
    private Boolean gender;
}
