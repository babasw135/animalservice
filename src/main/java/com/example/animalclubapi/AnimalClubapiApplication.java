package com.example.animalclubapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalClubapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnimalClubapiApplication.class, args);
    }

}
