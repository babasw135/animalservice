package com.example.animalclubapi.service;

import com.example.animalclubapi.Entity.Animal;
import com.example.animalclubapi.Entity.Member;
import com.example.animalclubapi.Model.Animal.AnimalRequest;
import com.example.animalclubapi.Repository.AnimalRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AnimalService {
    private final AnimalRepository animalRepository;

    public void setAnimal(Member member, AnimalRequest request){
        Animal addData = new Animal();
        addData.setMember(member);
        addData.setMonthlyPayment(request.);
    }
}
