package com.example.animalclubapi.service;

import com.example.animalclubapi.Entity.Member;
import com.example.animalclubapi.Model.Member.MemberRequest;
import com.example.animalclubapi.Repository.MemberRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
    return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberRequest request){
    Member addData = new Member();
    addData.setName(request.getName());
    addData.setPhoneNumber(request.getPhoneNumber());
    addData.setGender(request.getGender());

    memberRepository.save(addData);
    }
}
